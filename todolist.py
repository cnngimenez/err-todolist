from errbot import BotPlugin, botcmd, arg_botcmd
import logging
from entry import Entry
import time
from re import match


class TodoList(BotPlugin):
    def hour_callback(self):
        lst = Entry.get_with_deadlines()
        curtime = time.time()
        ttime = time.localtime(curtime)
        notify = True
        # If it is not 8:00 AM at least, don't send messages.
        # if 0 < ttime[3] < 8:
        #     notify = False
        if ttime[3] not in [8, 11, 14, 17, 20]:
            notify = False
        if ttime[4] > 10:
            notify = False

        if notify:
            for e in lst:
                if e.deadline <= curtime:
                    # Deadline Reached -> Send message!
                    self.send(
                        self.build_identifier(e.creator),
                        f"You have a pending TODO:\n{e}"
                    )

    def activate(self):
        """Triggers on plugin activation,

        Loads the old todo list from a file if it exists."""
        super(TodoList, self).activate()
        try:
            Entry.create_tables()
            self.start_poller(600, self.hour_callback)
        except IOError:
            logging.debug("Failed to open the DB file.")

    def deactivate(self):
        super(TodoList, self).deactivate()

    @botcmd
    def todo(self, mess, args):
        """Shorthand for !todo create"""
        return self.todo_create(mess, args)

    @botcmd
    def todo_pending(self, mess, args):
        """Show entries with passed deadlines"""
        lst = Entry.get_with_deadlines(username=str(mess.frm))
        curtime = time.time()
        ret = "Pending TODOes:"
        for e in lst:
            if e.deadline <= curtime:
                ret += f"\n{e}"
        return ret

    @botcmd(split_args_with=' ')
    def todo_deadline(self, mess, args):
        """
        Set a deadline to an entry.

        Recieves two parameters: id, deadline

        Deadline can be "tomorrow", "today", "Day-Month-Year Hour:Minutes"
        (Hour in 24 hours, Day and Month with ceroes).
        """
        ident = int(args[0])
        e = Entry.find(ident)

        if e is None:
            return f"Couldn't find entry ID {ident}."

        if args[1] == "tomorrow":
            e.deadline_tomorrow()
        elif args[1] == "today":
            e.deadline_today()
        else:
            e.deadline_str(args[1], args[2])

        e.save()
        return f"Deadline setted: \n{e}"

    @botcmd(split_args_with=' ')
    def todo_done(self, mess, args):
        """Set a TODO in Done state"""
        ident = int(args[0])
        e = Entry.find(ident)

        if e is None:
            return f"Couldn't find entry ID {ident}."

        e.done = True
        e.save()
        return f"Congratulations! 🎉 You've done [{e.ident}] {e.title}!"

    @arg_botcmd('--done', dest='list_done', type=bool, default=False)
    def todo_list(self, mess, list_done=False):
        """Lists all entries of the todo list"""
        entries = Entry.get_users_todo(str(mess.frm), list_done)

        ret = ""
        if len(entries) > 0:
            ret += "Listing all entries of the todo list:\n"
        else:
            ret += "No entries yet. Set some tasks, lazybone!"

        for item in entries:
            ret += str(item) + "\n"

        return ret

    @botcmd
    def todo_create(self, mess, args):
        """Creates a new entry on the todo list.

        Syntax: !todo create <title>.

        Add "today", "tomorrow" or a datetime like "20-01-2018 21:22". """

        e = Entry(args, str(mess.frm))

        mtoday = match('.*[\.,;: ]today[$\.,:; ].*', args)
        mtomorrow = match('.*[\.,:; ]tomorrow[$\.,:; ].*', args)
        mdate = match('.*[\.,;: ](\d\d-\d\d-\d\d\d\d) (\d\d:\d\d)[$\.,:; ].*',
                      args)
        if mtoday is not None:
            e.deadline_today()
        elif mtomorrow is not None:
            e.deadline_tomorrow()
        elif mdate is not None:
            e.deadline_str(mdate.group(1), mdate.group(2))

        e.save()

        s = f"Created a new entry with id {e.ident}"
        s += f", use !todo describe {e.ident}"
        s += " to add a detailed description."
        s += str(e)
        return s

    @botcmd(split_args_with=' ')
    def todo_remove(self, mess, args):
        """Removes an entry from the todo list.

        Syntax !todo remove <ID>. Get the right ID by using !todo list"""
        e = Entry.find(int(args[0]))

        if e is None:
            s = f"Couldn't find the todo list entry {args[0]}"
            s += ", sorry. Use !todo list to see all entries and their IDs."
            return s

        temp_title = e.title
        e.remove()

        s = f"Successfully removed entry [{args[0]}] : {temp_title}."
        return s

    @botcmd(split_args_with=' ')
    def todo_describe(self, mess, args):
        """Changes (and adds) the description of an entry.

        Syntax: !todo describe <ID> <description>.
        Get the right ID by using !todo list"""
        i = int(args[0])
        e = Entry.find(i)
        if e is None:
            s = "Couldn't find the todo list entry " + str(i)
            s += ", sorry. Use !todo list to see all entries and their IDs."
            return s

        e.description = ' '.join(args[1::])

        s = f"Successfully changed the description of entry [{i}] {e.title}."
        return s

    @botcmd(split_args_with=' ')
    def todo_assign(self, mess, args):
        """Assigns persons to an entry.

        Syntax: !todo assign <ID> <person_0> [person_1] ... [person_n].
        Use !todo list to see all entries and their IDs."""
        i = int(args[0])

        e = Entry.find(i)
        if e is None:
            s = "Couldn't find the todo list entry " + str(i)
            s += ", sorry. Use !todo list to see all entries and their IDs."
            return s

        e.assignees += args[1::]
        e.save()

        s = "Successfully assigned " + ", ".join(args[1::])
        s += " to [{i}] {e.title}."
        return s

    @botcmd(split_args_with=' ')
    def todo_unassign(self, mess, args):
        """Unassigns persons from an entry.

        Syntax: !todo unassign <ID> <person_0 [person_1] ... [person_n].
        Use !todo list to see all entries and their IDs."""
        i = int(args[0])
        e = Entry.find(i)

        if e is None:
            s = f"Couldn't find the todo list entry {i}"
            s += ", sorry. Use !todo list to see all entries and their IDs."
            return s

        e.assignees = [a for a in e.assignees
                       if a not in args[1::]]
        e.save()

        s = "Successfully unassigned " + ", ".join(args[1::])
        s += " from [{i}] {e.title}."
        return s

    @botcmd(split_args_with=' ')
    def todo_chtitle(self, mess, args):
        """Changes the title of an entry.

        Syntax: !todo chtitle <ID> <new_tile>."""
        i = int(args[0])

        e = Entry.find(i)

        if e is None:
            s = f"Couldn't find the todo list entry {i}"
            s += ", sorry. Use !todo list to see all entries and their IDs."
            return s

        temp_title = e.title
        e.title = " ".join(args[1::])
        e.save()

        s = f"Successfully changed the title from {temp_title} to {e.title}."
        return s


# Local Variables:
# fill-column: 80
# python-indent: 4
# End:
