# -*- coding: utf-8 -*-
'''
entry

Entry module

:copyright: 2018 Giménez, Christian
:author: Giménez, Christian
:license: GPL v3 (see COPYING.txt or LICENSE.txt file for more information)
'''
#
# entry.py
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3
import time
import datetime
from typing import List


class Entry:
    def __init__(self, title, creator, dbfile='todolist.sqlite3'):
        self.dbfile = dbfile
        self.ident = None
        self.title = title
        self.creator = creator
        self.description = "No description. :("
        self.done = False
        self.deadline = None
        self.timestamp = time.time()
        self.assignees = []

    def _save_assignees(self, con: sqlite3.Connection):
        """Save all assignees into de SQLite DB"""
        query = "INSERT INTO assignees VALUES (?, ?)"

        for person in self.assignees:
            con.execute(query, (self.ident, self.person))

    def deadline_today(self):
        t = time.time()
        t = t

        lt = time.localtime(t)
        seconds_hours = lt[3] * 3600
        seconds_mins = lt[4] * 60
        # Lo llevamos a 0:00
        t -= seconds_hours + seconds_mins + lt[5]
        # Lo llevamos a las 8 AM
        t += 28800
        self.deadline = t

    def deadline_tomorrow(self):
        t = time.time()
        t = t + 86400

        lt = time.localtime(t)
        seconds_hours = lt[3] * 3600
        seconds_mins = lt[4] * 60
        # Lo llevamos a 0:00
        t -= seconds_hours + seconds_mins + lt[5]
        # Lo llevamos a las 8 AM
        t += 28800
        self.deadline = t

    def deadline_str(self, datestr: str, timestr: str):
        sdate = datestr + " " + timestr
        t = time.strptime(sdate, "%d-%m-%Y %H:%M")
        t = time.mktime(t)
        self.deadline = t

    def save(self):
        """Save data into SQLite DB."""
        connection = sqlite3.connect(self.dbfile)

        query = "INSERT OR REPLACE INTO entries VALUES (?, ?, ?, ?, ?, ?, ?)"

        res = connection.execute(
            query,
            (self.ident, self.title, self.creator, self.description,
             self.done, self.deadline, self.timestamp))
        connection.commit()

        # Update identifier according to the saved one if not assined.
        if self.ident is None:
            self.ident = res.lastrowid

        self._save_assignees(connection)
        connection.close()

    def remove(self):
        if self.ident is None:
            return

        connection = sqlite3.connect(self.dbfile)
        query = "DELETE FROM entries WHERE id = ?"
        connection.execute(query, [self.ident])
        connection.commit()
        connection.close()

    def __str__(self) -> str:
        done = "✔"
        if not self.done:
            done = ""
        ret = f"[{self.ident}] {done}{self.title}:\n{self.description}"
        if len(self.assignees) > 0:
                ret += "\nAssigned to this entry: "
                ret += ", ".join(self.assignees)

        if self.deadline is not None:
            ddt = datetime.datetime.fromtimestamp(self.deadline)
            deadline = ddt.strftime('%d.%m.%Y %H:%M:%S')
            ret += f"\nDEADLINE: {deadline}"

        dt = datetime.datetime.fromtimestamp(self.timestamp)
        time = dt.strftime('%d.%m.%Y %H:%M:%S')
        ret += f"\n(by {self.creator} {time})"
        return ret

    def __repr__(self) -> str:
        return f"<Entry [{self.ident}] {self.title} {self.creator}>"

    @classmethod
    def create_tables(cls, dbfile='todolist.sqlite3'):
        con = sqlite3.connect(dbfile)

        """Create the SQLite DB tables"""
        con.execute(
            '''CREATE TABLE IF NOT EXISTS entries (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            title VARCHAR NOT NULL,
            creator VARCHAR NOT NULL, desc TEXT,
            done BOOLEAN, deadline DATETIME, timestamp DATETIME)''')
        con.execute(
            '''CREATE TABLE IF NOT EXISTS assignees (
            id INTEGER, assignee VARCHAR,
            CONSTRAINT 'pk-assignees' PRIMARY KEY (id, assignee),
            CONSTRAINT 'fk-assignees' FOREIGN KEY (id)
                REFERENCES entries(id)
                ON UPDATE CASCADE ON DELETE CASCADE)''')
        con.commit()

        con.close()

    @classmethod
    def all(cls, dbfile='todolist.sqlite3'):
        """Return all the entries"""
        con = sqlite3.connect(dbfile)

        r = con.execute(
            '''SELECT id, title, creator, desc, done, deadline, timestamp
            FROM entries''')
        lstout = []
        for t in r:
            entry = Entry.create_from_tuple(t)
            lstout.append(entry)

        con.close()
        return lstout

    @classmethod
    def find(cls, ident: int, dbfile='todolist.sqlite3') -> 'Entry':
        con = sqlite3.connect(dbfile)

        query = "SELECT id, title, creator, desc, done, deadline, timestamp "
        query += " FROM entries WHERE id = ?"
        q = con.execute(query, [ident])
        t = q.fetchone()
        e = Entry.create_from_tuple(t)

        con.close()
        return e

    @classmethod
    def get_users_todo(cls,
                       username: str,
                       list_done: bool=False,
                       dbfile='todolist.sqlite3') -> List['Entry']:
        """
        Get all ToDoes created by the provided user.
        """
        con = sqlite3.connect(dbfile)

        query = "SELECT id, title, creator, desc, done, deadline, timestamp "
        query += "FROM entries WHERE creator = ? AND done = ? "
        query += "ORDER BY timestamp DESC"

        lstout = []
        for t in con.execute(query, [username, list_done]):
            e = Entry.create_from_tuple(t)
            lstout.append(e)

        con.close()
        return lstout

    @classmethod
    def create_from_tuple(cls, t: tuple):
        """
        Create an entry from a tuple.

        Tuple must be
        (id, title, creator, description, done, deadline, timestamp).
        """
        e = Entry(t[1], t[2])
        e.ident = t[0]
        e.description = t[3]
        e.done = t[4]
        e.deadline = t[5]
        e.timestamp = t[6]
        return e

    @classmethod
    def get_with_deadlines(cls,
                           username: str=None,
                           dbfile='todolist.sqlite3') -> list:
        """
        Retrieve all entries with deadline and not done.
        """
        con = sqlite3.connect(dbfile)
        q = None
        if username is None:
            query = """SELECT id, title, creator, desc, done, deadline, timestamp
            FROM entries WHERE done = 0 AND deadline != null"""
            q = con.execute(query)
        else:
            query = """SELECT id, title, creator, desc, done, deadline, timestamp
            FROM entries WHERE done = 0 AND deadline NOTNULL AND creator = ?"""
            q = con.execute(query, [username])
        lst = []
        lst_tuples = q.fetchall()
        for t in lst_tuples:
            lst.append(Entry.create_from_tuple(t))

        con.close()
        return lst
